import subprocess

# subprocess.call
# subprocess.check_call
# subprocess.check_output
# subprocess.Popen

try:
    completed = subprocess.run(["false"],
                               capture_output=True, text=True, check=True)
    print("args", completed.args)
    print("returncode", completed.returncode)
    print("stderr", completed.stderr)
    print("stdout", completed.stdout)
    if completed.returncode != 0:
        print("stderr", completed.stderr)
except subprocess.CalledProcessError as ex:
    print(ex)
