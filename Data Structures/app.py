from pprint import pprint

sentence = "This is a common interview question"

# letters = [*sentence]
# print(letters)

# lettersCount = {}
# for l in letters:
#     lettersCount[l] = 0

# for l in letters:
#     lettersCount[l] = lettersCount[l] + 1

# for key, value in lettersCount.items():
#     print(key, value)

char_frequency = {}
for char in sentence:
    if char in char_frequency:
        char_frequency[char] += 1
    else:
        char_frequency[char] = 1

char_frequency_sorted = sorted(char_frequency.items(),
                               key=lambda kv: kv[1],
                               reverse=True)
print(char_frequency_sorted[0])
